import numpy as np
from numba import vectorize, float64, float32, uint8, uint16, guvectorize, int32, int64
import open3d as o3d


@guvectorize([(float32[:, :], float32, float32, float32, float32[:, :]),
              (float64[:, :], float64, float64, float64, float64[:, :])],
             '(n, m),(),(),()->(n, m)', nopython=True, cache=True)
def calculate_rotation_matrix_opt(xy, omega_deg, phi_deg, kappa_deg, res):
    "Optimized calculation of the rotation matrix. This is about 25x faster than the last implementation"
    omega = np.deg2rad(omega_deg)
    phi = np.deg2rad(phi_deg)
    kappa = np.deg2rad(kappa_deg)
    # Ugly but fast
    res[0][0] = np.cos(phi) * np.cos(kappa)
    res[0][1] = np.sin(omega) * np.sin(phi) * np.cos(kappa) - np.cos(omega) * np.sin(kappa)
    res[0][2] = np.cos(omega) * np.sin(phi) * np.cos(kappa) + np.sin(omega) * np.sin(kappa)
    res[1][0] = np.cos(phi) * np.sin(kappa)
    res[1][1] = np.sin(omega) * np.sin(phi) * np.sin(kappa) + np.cos(omega) * np.cos(kappa)
    res[1][2] = np.cos(omega) * np.sin(phi) * np.sin(kappa) - np.sin(omega) * np.cos(kappa)
    res[2][0] = -np.sin(phi)
    res[2][1] = np.sin(omega) * np.cos(phi)
    res[2][2] = np.cos(omega) * np.cos(phi)


def calculate_rotation_matrix(omega_deg, phi_deg, kappa_deg):
    xy = np.empty((3, 3)).astype(np.float32)
    return calculate_rotation_matrix_opt(xy, omega_deg, phi_deg, kappa_deg)


@vectorize([float32(float32, float32, float32),
            float64(float64, float64, float64)], nopython=True, cache=True)
def radial(x, y, z):
    return np.sqrt(x ** 2 + y ** 2 + z ** 2)


@vectorize([float32(float32, float32, float32),
            float64(float64, float64, float64)], nopython=True, cache=True)
def theta(x, y, z):
    r = radial(x, y, z)
    return np.degrees(np.arccos(z / r))


@vectorize([float32(float32, float32),
            float64(float64, float64)], nopython=True, cache=True)
def phi(x, y):
    if x >= 0:
        return np.degrees(np.arctan(y / x))
    else:
        return np.degrees(np.arctan(y / x) + np.pi)


@guvectorize([(float32[:, :], uint16, uint16, float32[:, :]),
              (float32[:, :], uint16, uint16, float32[:, :]),
              (float64[:, :], uint16, uint16, float64[:, :])],
             '(n_points, m),(),()->(n_points, m)', nopython=True, cache=True)
def translate_xy_to_spherical(xy, image_w, image_h, res):
    for i in range(xy.shape[0]):
        res[i][0] = (xy[i][0] / (image_w / 360.0)) - 180.0  # H
        res[i][1] = ((xy[i][1] / (image_h / 180.0)) - 90.0) / -1  # V


@guvectorize([(float32[:, :], uint16, uint16, int32[:, :]),
              (float64[:, :], uint16, uint16, int64[:, :])],
             '(n_points, m),(),()->(n_points, m)', nopython=True, cache=True)
def translate_spherical_to_xy(hv, image_w, image_h, res):
    # It is 40 times slower without the loop.
    for i in range(hv.shape[0]):
        res[i][1] = (((hv[i][0] % 360.0) + 180.0) * (image_h / 180.0)) - image_h + 0.5  # 0.5 to round up or down
        # res[i][0] = (((hv[i][1] * -1) + 90.0) * (image_w / 360.0)) + image_w / 2.0 + 0.5
        res[i][0] = (360 - hv[i][1]) * (image_w / 360.0) + image_w / 2.0 + 0.5


@guvectorize([(float32[:, :], float32[:, :]),
              (float64[:, :], float64[:, :])],
             '(n_points, m)->(n_points, m)', nopython=True, cache=True)
def cartesian_to_spherical(xyz, rtp):
    for i in range(xyz.shape[0]):
        rtp[i][0] = np.sqrt(xyz[i][0] ** 2 + xyz[i][1] ** 2 + xyz[i][2] ** 2)
        rtp[i][1] = np.degrees(np.arccos(xyz[i][2] / rtp[i][0]))
        if xyz[i][0] >= 0:
            rtp[i][2] = np.degrees(np.arctan(xyz[i][1] / xyz[i][0]))
        else:
            rtp[i][2] = np.degrees(np.arctan(xyz[i][1] / xyz[i][0]) + np.pi)


@guvectorize([(float32[:], int32[:, :], float32, float32[:, :], float32[:, :]),
              (float64[:], int64[:, :], float64, float64[:, :], float64[:, :])],
             '(n_points),(n_points, n_dim), (), (w, h)->(w, h)', nopython=True, cache=True)
def computePanoPixelPoints(rs, xy, radius, panoPixelPoints, res):
    photo_height_px = panoPixelPoints.shape[0]
    res[:, :] = 0
    for i in range(xy.shape[0]):
        y = xy[i][1]
        x = xy[i][0]
        r = rs[i]
        if y < photo_height_px and res[y, x] == 0.0:
            res[y, x] = r
        elif 0.1 < r < radius and y < photo_height_px and r < res[y, x]:
            res[y, x] = r


@guvectorize([(uint8[:, :], uint8[:, :, :], uint8[:, :, :]),
              (float32[:, :], uint8[:, :, :], uint8[:, :, :]),
              (float64[:, :], uint8[:, :, :], uint8[:, :, :])],
             '(n_h, n_w),(h, w, c)->(h, w, c)', nopython=True, cache=True)
def computePixelArray(panoPixelPoints, pixelsarray, res):
    for (pixel_y, pixel_x), pixel in np.ndenumerate(panoPixelPoints):
        if pixel == 0:
            res[pixel_y, pixel_x, 0] = 0
            res[pixel_y, pixel_x, 1] = 0
            res[pixel_y, pixel_x, 2] = 0
        else:
            # Quantization and color offset
            # Bit masking
            m = 100000 + pixel * 1000
            r = np.floor_divide(m, 65536)
            g = np.floor_divide(m, 256) - r * 256
            b = np.floor(m) - r * 65536 - g * 256

            res[pixel_y, pixel_x, 0] = r
            res[pixel_y, pixel_x, 1] = g
            res[pixel_y, pixel_x, 2] = b


def computePano(xyz, panocam, photo_width_px: int, photo_height_px: int, radius: float, RotMat):
    """
    :param xyz: Numpy array of point cloud of shape (num_points, 3)
    :param panocam:  Numpy array of the coordinate of pano cam (3,)
    :param photo_width_px: width of photo (int)
    :param photo_height_px: height of phot (int)
    :param radius: radius range (float)
    :return: panorama image (photo_height_px, photo_width_px, 3)
    """
    dpoint = xyz - panocam
    dpoint_rot = np.transpose(np.asarray(np.matmul(RotMat, np.transpose(np.asmatrix(dpoint)))))
    rtp = cartesian_to_spherical(dpoint_rot)

    xy = translate_spherical_to_xy(rtp[:, 1:], photo_width_px, photo_height_px)
    panoPixelPoints = np.empty((photo_height_px, photo_width_px)).astype(np.float32)
    panoPixelPoints = computePanoPixelPoints(rtp[:, 0], xy, radius, panoPixelPoints)
    pixelsarray = np.empty((photo_height_px, photo_width_px, 3)).astype(np.uint8)
    pixelsarray = computePixelArray(panoPixelPoints, pixelsarray)
    return pixelsarray


def load_file(pcdpath):
    return o3d.io.read_point_cloud(pcdpath)
