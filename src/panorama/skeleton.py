# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = panorama.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""
import psutil
import argparse
import copy
import sys
import logging
from tqdm import tqdm
import numpy as np
import pandas as pd
import open3d as o3d
from PIL import Image
from joblib import Parallel, delayed
from .generate import computePano, load_file, calculate_rotation_matrix
from pathlib import Path
# from panorama import __version__
import multiprocessing

__author__ = "Andrij David"
__copyright__ = "Andrij David"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Convert point cloud to panorama")
    # parser.add_argument(
    #    "--version",
    #    action="version",
    #    version="panorama {ver}".format(ver=__version__))
    parser.add_argument(
        "-w",
        "--width",
        dest="width",
        default=1024,
        help="set the width of the panorama",
        type=int
    )
    parser.add_argument(
        "--height",
        dest="height",
        default=512,
        help="set the width of the panorama",
        type=int
    )
    parser.add_argument(
        "-r",
        "--radius",
        dest="radius",
        help="set the filter radius",
        default=2000,
        type=float
    )
    parser.add_argument(
        "-rr",
        "--radius-removal",
        dest="radiusforremoval",
        help="set the radius for removal",
        default=1000 * 100,
        type=float
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        help="set the output directory",
        default="output/",
        type=str
    )
    parser.add_argument(
        "-x",
        "--offset-x",
        dest="x",
        help="set the offset of x",
        default=257400,
        type=int
    )
    parser.add_argument(
        "-y",
        "--offset-y",
        dest="y",
        help="set the offset of y",
        default=1238400,
        type=int
    )
    parser.add_argument(
        "-i",
        "--input",
        dest="input",
        help="set the input csv containing the frame",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-p",
        "--ply",
        dest="ply",
        help="set the input point clouds",
        required=True,
        nargs='+'
    )
    parser.add_argument(
        "-a",
        "--align-with-img",
        action='store_true',
        dest="align",
        help="align image",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO)

    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")
    cam = pd.read_csv(args.input)
    to_global_offset_x, to_global_offset_y = args.x, args.y
    out = Path(args.output)
    out.mkdir(parents=True, exist_ok=True)
    xyz = load_ply(args.ply)
    available_memory = psutil.virtual_memory().available * 0.95  # Use only 95% of immediately available memory
    process = int(available_memory // 0.5e+10)  # Each process needs a max of 15GB
    print(process)
    results = Parallel(n_jobs=process, max_nbytes=available_memory // process )(
        delayed(generate_pano)(xyz, row['X [m]'], row['Y [m]'], row['Height [m]'], row["Frame"], row["Omega [°]"],
                               row["Phi [°]"], row["Kappa [°]"], args.width, args.height,
                               args.radius, args.radiusforremoval, to_global_offset_x, to_global_offset_y, args.output,
                               args.align) for
        j, row in tqdm(cam.iterrows(), total=len(cam)))

    # for j, row in tqdm(cam.iterrows(), total=len(cam)):
    #     generate_pano(xyz, row['X [m]'], row['Y [m]'], row['Height [m]'], row["Frame"], row["Omega [°]"],
    #                   row["Phi [°]"], row["Kappa [°]"], args.width, args.height,
    #                   args.radius, args.radiusforremoval, to_global_offset_x, to_global_offset_y, args.output,
    #                   args.align)
    #     break
    _logger.info("Script ends here")


def load_ply(plies):
    xyz = None
    for f in tqdm(plies):
        pcd_load = load_file(f)
        xyz_load = np.asarray(pcd_load.points, dtype=np.float32)
        if xyz is None:
            xyz = xyz_load
        else:
            xyz = np.concatenate((xyz, xyz_load), axis=0)
    return xyz


def generate_pano(xyz, x, y, z, frame, omega_deg, phi_deg, kappa_deg, width, height, radius, radiusforremoval,
                  to_global_offset_x, to_global_offset_y, output, align):
    if align:
        R = calculate_rotation_matrix(omega_deg, phi_deg, kappa_deg)
    else:
        R = np.matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    panocam_global_position = [x, y, z]
    panocam = np.array(
        [panocam_global_position[0] + to_global_offset_x,
         panocam_global_position[1] + to_global_offset_y,
         panocam_global_position[2]], dtype=np.float32)
    points = extract_points(panocam, radiusforremoval, xyz)
    del xyz
    generate_pano_from_numpy(points, panocam, width, height, radius, R, name=frame, output_dir=output)


def extract_points(panocam, radiusforremoval, xyz):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)
    _, pt_map = pcd.hidden_point_removal(panocam, radiusforremoval)
    pcd1 = pcd.select_by_index(pt_map)
    points = np.asarray(pcd1.points, dtype=np.float32)
    return points


def generate_pano_from_numpy(points, panocam, photo_width_px, photo_height_px, radius, RotMat, name, output_dir):
    pixelsarray = computePano(points, panocam, photo_width_px, photo_height_px, radius, RotMat)
    new_image = Image.fromarray(pixelsarray)
    name_str = ('000000' + str(name))[-6:]
    new_image.save(f"{output_dir}/{name_str}.png")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
