========
panorama
========


Convert pointcloud to a panorama image from a camera perspective

Installation
============
Requirements:
 - python >=3.7

``pip install panorama --extra-index-url https://gitlab.com/api/v4/projects/31364389/packages/pypi/simple``

Usage
=========

As a library:

``
from panorama import computePano
img = computePano(xyz, panocam, photo_width_px: int, photo_height_px: int, radius: float)
``


    :xyz: Numpy array of point cloud of shape (num_points, 3)
    :panocam:  Numpy array of the coordinate of pano cam (3,)
    :photo_width_px: width of photo (int)
    :photo_height_px: height of phot (int)
    :radius: radius range (float)
    :return: panorama image (photo_height_px, photo_width_px, 3)

Command line parameter
``panorama -h``
``usage: panorama [-h] [--version] [-w WIDTH] [--height HEIGHT] [-r RADIUS]
                [-rr RADIUSFORREMOVAL] [-o OUTPUT] [-x X] [-y Y] -i INPUT -p
                PLY [PLY ...] [-v] [-vv]

Convert point cloud to panorama

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -w WIDTH, --width WIDTH
                        set the width of the panorama
  --height HEIGHT       set the width of the panorama
  -r RADIUS, --radius RADIUS
                        set the filter radius
  -rr RADIUSFORREMOVAL, --radius-removal RADIUSFORREMOVAL
                        set the radius for removal
  -o OUTPUT, --output OUTPUT
                        set the output directory
  -x X, --offset-x X    set the offset of x
  -y Y, --offset-y Y    set the offset of y
  -i INPUT, --input INPUT
                        set the input csv containing the frame
  -p PLY [PLY ...], --ply PLY [PLY ...]
                        set the input point clouds
  -v, --verbose         set loglevel to INFO
  -vv, --very-verbose   set loglevel to DEBUG``

For example: ``panorama -i data/panoDB_JTSK03_Bpv.csv -p data/Hrasovik/output_cloud_0_JTSK03_Bpv.pcd data/Hrasovik/output_cloud_1_JTSK03_Bpv.pcd data/Hrasovik/output_cloud_2_JTSK03_Bpv.pcd``



